package com.example.kniffel;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import DiceController.Dice;
import GameManager.ResultChecker;
import PlayerController.Player;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class TestResultChecks {

    private static Player player;
    private static Player player2;
    private static Player player3;
    private static Player player4;
    private static  Player player5;
    private static Player player6;
    private static Player player7;
    private static Player player8;
    private ResultChecker resultChecker = new ResultChecker();

    @BeforeClass
    public static void prepare() {
        player = new Player("test");
        player.getPlayersDiceList().set(0, new Dice(1));
        player.getPlayersDiceList().set(1, new Dice(2));
        player.getPlayersDiceList().set(2, new Dice(3));
        player.getPlayersDiceList().set(3, new Dice(4));
        player.getPlayersDiceList().set(4, new Dice(5));

        player2 = new Player("test2");
        player2.getPlayersDiceList().set(0, new Dice(1));
        player2.getPlayersDiceList().set(1, new Dice(2));
        player2.getPlayersDiceList().set(2, new Dice(3));
        player2.getPlayersDiceList().set(3, new Dice(4));
        player2.getPlayersDiceList().set(4, new Dice(6));

        player3 = new Player("test3");
        player3.getPlayersDiceList().set(0, new Dice(4));
        player3.getPlayersDiceList().set(1, new Dice(4));
        player3.getPlayersDiceList().set(2, new Dice(4));
        player3.getPlayersDiceList().set(3, new Dice(5));
        player3.getPlayersDiceList().set(4, new Dice(1));

        player4 = new Player("test4");

        player4.getPlayersDiceList().set(0, new Dice(4));
        player4.getPlayersDiceList().set(1, new Dice(4));
        player4.getPlayersDiceList().set(2, new Dice(4));
        player4.getPlayersDiceList().set(3, new Dice(4));
        player4.getPlayersDiceList().set(4, new Dice(1));

        player5 = new Player("test5");
        player5.getPlayersDiceList().set(0,new Dice(1));
        player5.getPlayersDiceList().set(1,new Dice(1));
        player5.getPlayersDiceList().set(2,new Dice(1));
        player5.getPlayersDiceList().set(3,new Dice(1));
        player5.getPlayersDiceList().set(4,new Dice(1));

        player6= new Player("test6");

        player6.getPlayersDiceList().set(0,new Dice(1));
        player6.getPlayersDiceList().set(1,new Dice(1));
        player6.getPlayersDiceList().set(2,new Dice(2));
        player6.getPlayersDiceList().set(3,new Dice(2));
        player6.getPlayersDiceList().set(4,new Dice(2));

        player7 = new Player("test7");

        player7.getPlayersDiceList().set(0,new Dice(3));
        player7.getPlayersDiceList().set(1,new Dice(3));
        player7.getPlayersDiceList().set(2,new Dice(3));
        player7.getPlayersDiceList().set(3,new Dice(2));
        player7.getPlayersDiceList().set(4,new Dice(5));


        player8 = new Player("test8");

        player8.getPlayersDiceList().set(0,new Dice(2));
        player8.getPlayersDiceList().set(1,new Dice(2));
        player8.getPlayersDiceList().set(2,new Dice(1));
        player8.getPlayersDiceList().set(3,new Dice(4));
        player8.getPlayersDiceList().set(4,new Dice(3));


    }

    @Test
    public void checkStaße() {
        Assert.assertTrue(resultChecker.checkStreet(player.getPlayersDiceList(),5));
        Assert.assertTrue(resultChecker.checkStreet(player2.getPlayersDiceList(),4));
        Assert.assertFalse(resultChecker.checkStreet(player2.getPlayersDiceList(),5));
        Assert.assertFalse(resultChecker.checkStreet(player3.getPlayersDiceList(),4));
        Assert.assertTrue(resultChecker.checkStreet(player8.getPlayersDiceList(),4));
    }
    @Test
    public void checkPasch(){
        assertEquals(0, resultChecker.checkPasch(player.getPlayersDiceList(), 3));
        assertEquals(0, resultChecker.checkPasch(player.getPlayersDiceList(), 4));
        assertEquals(16, resultChecker.checkPasch(player4.getPlayersDiceList(), 4));
        assertEquals(12, resultChecker.checkPasch(player3.getPlayersDiceList(), 3));
        assertEquals(6,resultChecker.checkPasch(player6.getPlayersDiceList(),3));           //siehe TODO in ResultChecker
    }

    @Test
    public void checkKniffel(){
        Assert.assertTrue(resultChecker.checkKniffel(player5.getPlayersDiceList()));
        Assert.assertFalse(resultChecker.checkKniffel(player.getPlayersDiceList()));
    }

    @Test
    public void checkFullHouse(){
        Assert.assertTrue(resultChecker.checkFullHouse(player6.getPlayersDiceList()));
        Assert.assertFalse(resultChecker.checkFullHouse(player.getPlayersDiceList()));
        Assert.assertFalse(resultChecker.checkFullHouse(player7.getPlayersDiceList()));
        Assert.assertFalse(resultChecker.checkFullHouse(player3.getPlayersDiceList()));

    }
}