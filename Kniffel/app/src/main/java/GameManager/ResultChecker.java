package GameManager;

import java.util.List;

import DiceController.Dice;

public class ResultChecker {

    /**
     * Checking For a FullHouse
     * @param dicesToCheck Player Dices to check
     * @return is it a FullHouse?
     */
    public boolean checkFullHouse(List<Dice> dicesToCheck) {
        int list1 = 0;
        int list2 = 0;
        Dice second = null;
        for (Dice dice : dicesToCheck) {
            if (dice.getValue() == dicesToCheck.get(0).getValue()) {
                list1++;
            } else if (second == null) {
                second = dice;
                list2++;
            } else if (second.getValue() == dice.getValue()) {
                list2++;
            }
        }
        return (list1 == 2 || list2 == 2) && (list1 == 3 || list2 == 3);

    }

    /**
     * Checking for a street
     * @param dicesToCheck Player Dices to check
     * @param sizeStrasse size of the street to check
     * @return is it a street with the fitting size
     */
    public boolean checkStreet(List<Dice> dicesToCheck, int sizeStrasse) {
        dicesToCheck.sort((o1, o2) -> o1.getValue() - o2.getValue());
        int realSize = 1;
        for (int i = 1; i < dicesToCheck.size(); i++) {
            if (dicesToCheck.get(i).getValue() - dicesToCheck.get(i - 1).getValue() == 1) {
                realSize++;
            }
        }
        return sizeStrasse <= realSize;
    }

    /**
     * Checking for same dices
     * @param dicesToCheck Player Dices to check
     * @param sizePasch how many same dices
     * @return are dices same
     */
    public int checkPasch(List<Dice> dicesToCheck, int sizePasch) {
        int value = 0;
        dicesToCheck.sort(((o1, o2) -> o1.getValue() - o2.getValue()));
        if (!checkFullHouse(dicesToCheck)) {
            int realSize = 1;
            for (int i = 1; i < dicesToCheck.size(); i++) {
                if (dicesToCheck.get(i).getValue() == dicesToCheck.get(i - 1).getValue()) {
                    realSize++;
                    value = dicesToCheck.get(i).getValue();                             //Wird unnötigerweise immer upgedated
                }
            }
            if (sizePasch > realSize) {
                return 0;
            }
        } else {
            if (sizePasch == 4) {
                return 0;
            }
            if (dicesToCheck.get(0).getValue() == dicesToCheck.get(2).getValue()) {
                value = dicesToCheck.get(0).getValue();
            } else {
                value = dicesToCheck.get(dicesToCheck.size() - 1).getValue();
            }
        }
        return value * sizePasch;
    }

    /**
     * Checking if all dices are the same
     * @param diceList Player Dices to check
     * @return Are all Dices the same
     */
    public boolean checkKniffel(List<Dice> diceList) {
        return 5 == diceList.stream().filter(dice -> dice.getValue() == diceList.get(0).getValue()).count();

    }
}
