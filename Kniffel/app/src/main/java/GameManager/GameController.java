package GameManager;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.example.kniffel.DiceThrowActivity;

import java.util.ArrayList;
import java.util.List;


import PlayerController.Player;

    public class GameController {

        private List<Player> playerList = new ArrayList<>();
        private DiceThrowActivity diceThrowActivity;
        private int currentPlayer = 0;

        public GameController(List<String> playerNames) {
            this.diceThrowActivity = new DiceThrowActivity();
            playerNames.forEach(name -> {
                if (!name.equals("")) playerList.add(new Player(name));
            });
            try {

                initializePlayer();
            } catch (IndexOutOfBoundsException i) {
                Log.d("Game Controller", "Keine Namen für Spieler angegeben müssen nachträglich auf Player geändert werden");
            }
        }


        /**
         * Checking if the game is finished
         * @param pointsViewList ViewList, that contains the key to access the Categories of a plaer
         * @param applicationContext Context needed to find the entry names of the TextViews when searched via ID
         * @return game is finished or not
         */
        public boolean checkFinished(List<TextView> pointsViewList, Context applicationContext) {
            if (playerList.get(playerList.size() - 1).getTablePointController().allValuesSet(pointsViewList,applicationContext)) {
                playerList.sort((player1, player2) -> player2.getTablePointController().getTotalPoints() - player1.getTablePointController().getTotalPoints());
                Log.d("GameController" , " The Winner is: " + playerList.get(0).getName());
                return true;
        }
        return false;

    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public Player getThisPlayer() {
        return playerList.get(currentPlayer);
    }

        /**
         * Initalizing the Player the player in the Dice-Throw-Activity
         */
    private void initializePlayer()  {
        diceThrowActivity.setCurrentPlayer(playerList.get(currentPlayer));
    }


        /**
         * Resetting to the First Player
         */
    private void resetCurrentPlayer(){
            currentPlayer = 0;
    }

        /**
         * Moving on to the next player
         */
    public void nextPlayer() {
        currentPlayer++;
        System.out.println(playerList.size());
        if (currentPlayer >= playerList.size()) {
            resetCurrentPlayer();
        }

        initializePlayer();
    }
}
