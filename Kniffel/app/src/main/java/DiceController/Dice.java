package DiceController;

import java.util.Random;

public class Dice {

    private int value;
    private String pictureURL;
    private String id;
    public int getValue() {
        return value;
    }

    /**
     * Rolling the dices with a random value + setting the fitting Picture-URL
     */
    public void rollDice() {
        Random random = new Random();
        value =random.nextInt(6)+1;
        pictureURL = "dice"+value;
    }

    public Dice(int value){
        this.value = value;
    }
    public Dice(String id){
        this.id = id;
        this.pictureURL=id;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public String getId() {
        return id;
    }
}
