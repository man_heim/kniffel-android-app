package com.example.kniffel;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


import java.util.List;

import PlayerController.Player;

public class WinnerScreenControl extends AppCompatActivity {

    private TextView ersterView, zweiterView, dritterView;
    private String erster = "";
    private String zweiter = "";

    /**
     * Visualizing names and points for first, second, third place and other places
     */
    @SuppressLint("SetTextI18n")
    public void setWinners() {
        List<Player> winnerList = StartActivity.getGameController().getPlayerList();
        erster = winnerList.get(0).getName() + "\n" + "Punkte: " + winnerList.get(0).getTablePointController().getTotalPoints();
        ersterView.setText(erster);
        zweiter = winnerList.get(1).getName() + "\n" + "Punkte: " + winnerList.get(1).getTablePointController().getTotalPoints();
        zweiterView.setText(zweiter);
        if (winnerList.size() > 2) {
            String dritter = winnerList.get(2).getName() + "\n" + "Punkte: " + winnerList.get(2).getTablePointController().getTotalPoints();
            dritterView.setText(dritter);
            if(winnerList.size()>3){
                String restliche = "";
                StringBuilder stringBuilder = new StringBuilder(restliche);
                for (int i = 0; i < winnerList.size(); i++) {
                   stringBuilder= stringBuilder.append(winnerList.get(i).getName()).append(" Punkte: ").append(winnerList.get(i).getTablePointController().getTotalPoints()).append("\n");
                }
               TextView restlicheView = findViewById(R.id.restlicheView);
                restlicheView.setText(stringBuilder);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner_screen);
        ersterView = findViewById(R.id.ersterView);
        zweiterView = findViewById(R.id.zweiterView);
        dritterView = findViewById(R.id.dritterView);
        Log.d("erster Create: ", " Erster" + erster);
        Log.d("zweiter Create: ", "zweiter " + zweiter);
        setWinners();
        Button exit = findViewById(R.id.buttonBeenden2);
        exit.setOnClickListener(v->finish());
        Button buttonNewGame = findViewById(R.id.buttonNeuesSpiel);
        buttonNewGame.setOnClickListener(v->{
            Intent intent = new Intent(this, StartActivity.class);
            startActivity(intent);
        });
    }

}
