package com.example.kniffel;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.util.Objects;

public class DeletingDialog extends AppCompatDialogFragment {

    /**
     * Assigning context as OnDialogAcceptedListener
     * @param context context which should be assigned
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            onDialogAcceptedListener = (OnDialogAcceptedListener) context;
        }catch (ClassCastException c){
            throw  new ClassCastException(context.toString() + " must implement OnDialogAcceptedListener");
        }
    }

    private String fieldName;
    private OnDialogAcceptedListener onDialogAcceptedListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.delete_dialog, null);

        builder.setView(view);
        builder.setTitle("Streichen");
        builder.setMessage("Wollfen sie Feld " + fieldName + " streichen?");
        builder.setNegativeButton("Abbrechen", (dialog, which) -> {
        });
        builder.setPositiveButton("Streichen", ((dialog, which) -> onDialogAcceptedListener.dialogAccepted(true)));
        return builder.create();
    }

    /**
     *Setting the Name of the field for the dialog output
     * @param fieldName Name of the Field to be set
     */
    void setFieldName(String fieldName) {
        fieldName = fieldName.toLowerCase();
        fieldName = fieldName.substring(0,1).toUpperCase();
        this.fieldName = fieldName;
    }

    public interface OnDialogAcceptedListener {
        void dialogAccepted(boolean accepted);
    }
}
