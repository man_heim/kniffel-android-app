package com.example.kniffel;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import DiceController.Dice;
import GameManager.GameController;
import PlayerController.Player;

public class DiceThrowActivity extends AppCompatActivity {

    private Button exit, throwDices, playerTable;
    private TextView anzahlWuerfe, viewSpieler, wurfCounter;
    static GameController gameController;
    private List<ImageView> diceViewList = new ArrayList<>();
    private HashMap<String, Dice> diceMap = new HashMap<String, Dice>();
    private List<ImageView> selectedDices = new ArrayList<>();
    private Animation animation;
    private Player currentPlayer;


    /**
     * Setting OnClickListeners
     */
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dice_throw_activity);
        currentPlayer = gameController.getThisPlayer();
        initViews();
        if(Integer.parseInt((String)wurfCounter.getText()) < 3){
            if(Integer.parseInt((String)wurfCounter.getText())<1){
                throwDices.setClickable(false);
                throwDices.setVisibility(View.INVISIBLE);
                wurfCounter.setText("");
                anzahlWuerfe.setText("Keine Würfe mehr");
            }
            playerTable.setVisibility(View.VISIBLE);
            playerTable.setOnClickListener(v1 -> {
                selectedDices.forEach(dice -> dice.setTag(R.id.selected,"unclicked"));
                selectedDices.clear();
                Intent intent = new Intent(this, PlayerValueTable.class);
                startActivity(intent);
            });
        }
        viewSpieler.setText(currentPlayer.getName());
        animation = AnimationUtils.loadAnimation(DiceThrowActivity.this, R.anim.shake);
        diceMap = createDiceList(gameController.getThisPlayer().getPlayersDiceList());
        exit.setOnClickListener(v -> {
            Intent intent = new Intent(this, StartActivity.class);
            startActivity(intent);
        });
        throwDices.setOnClickListener(v -> {
            if(wurfCounter.getText().equals("3")&&selectedDices.size()<5){
                TextView meldung = findViewById(R.id.meldung);
                meldung.setText(R.string.meldungWurf);
            }else {
                rollDices();
                int count = Integer.parseInt((String) wurfCounter.getText());
                count--;
                if (count <= 0) {
                    Intent intent = new Intent(this, PlayerValueTable.class);
                    startActivity(intent);
                } else if (count < 3) {
                    playerTable.setVisibility(View.VISIBLE);
                    playerTable.setOnClickListener(v1 -> {
                        Intent intent = new Intent(this, PlayerValueTable.class);
                        startActivity(intent);
                    });
                }
                currentPlayer.reduceWurfCounter();
                wurfCounter.setText(String.valueOf(count));
                selectedDices.forEach(diceView -> {
                    diceView.setBackgroundResource(android.R.color.transparent);
                    diceView.setTag(R.id.selected, "unclicked");
                });
                selectedDices.clear();
            }
        });
    }

    /**
     * Copying the Players dice List in an local HashMap with the String-ID as Key for better Look-Ups
     * @param dices Dice List of the current Player
     * @return  HashMap with Dice-ID an the Dice of the Player
     */
    private HashMap<String, Dice> createDiceList(List<Dice> dices) {
        HashMap<String, Dice> diceMap = new HashMap<String, Dice>();
        dices.forEach(dice -> diceMap.put(dice.getId(), dice));
        return diceMap;
    }

    /**
     * Rolling The Selected Dices
     */
    private void rollDices() {
        Log.d("Rolling dices of player ",currentPlayer.getName());
        selectedDices.forEach(diceView -> {
            Log.d("Selected Dice ",diceView.toString());
            Dice d = diceMap.get(diceView.getTag(R.id.initalID).toString());
            Log.d("Selected Dice has value: ",Integer.toString(d.getValue()));
            Log.d("Dice to choose",d.getId());
            d.rollDice();
            Log.d("Value now",String.valueOf(d.getValue()));
            setDiceImgURL(d);
        });
    }

    /**
     * Setting the correct Image-URL for dices, so they have the real value
     * @param dice  Dice where URL has to be changed
     */
    private void setDiceImgURL(Dice dice) {
        selectedDices.forEach(diceView -> {
            System.out.println(getResources().getResourceEntryName(diceView.getId()));
            if (getResources().getResourceEntryName(diceView.getId()).equals((dice.getId()))) {
                switch (dice.getValue()) {
                    case 1:
                        diceView.setImageResource(R.drawable.dice1);
                        break;
                    case 2:
                        diceView.setImageResource(R.drawable.dice2);
                        break;
                    case 3:
                        diceView.setImageResource(R.drawable.dice3);
                        break;
                    case 4:
                        diceView.setImageResource(R.drawable.dice4);
                        break;
                    case 5:
                        diceView.setImageResource(R.drawable.dice5);
                        break;
                    case 6:
                        diceView.setImageResource(R.drawable.dice6);
                        break;
                }
            }
            diceView.startAnimation(animation);
        });
    }

    /**
     * Select and deselect the clicked dice
     * @param diceView ImageView of the clicked Dice
     */
    private void selectDice(ImageView diceView) {
        if (diceView.getBackground() == null || "unclicked".equals(diceView.getTag(R.id.selected))) {
            diceView.setBackgroundResource(R.drawable.imageviewborder);
            diceView.setTag(R.id.selected, "selected");
            selectedDices.add(diceView);
        } else {
            diceView.setBackgroundResource(android.R.color.transparent);
            diceView.setTag(R.id.selected, "unclicked");
            selectedDices.remove(diceView);
        }
    }

    public static GameController getGameController() {
        return gameController;
    }


    private void initViews() {
        exit = findViewById(R.id.beenden);
        throwDices = findViewById(R.id.würfelnButton);
        anzahlWuerfe = findViewById(R.id.viewAnzahlWürfe);
        viewSpieler = findViewById(R.id.viewSpieler);
        ImageView dice1 = findViewById(R.id.dice1);
        ImageView dice2 = findViewById(R.id.dice2);
        ImageView dice3 = findViewById(R.id.dice3);
        ImageView dice4 = findViewById(R.id.dice4);
        ImageView dice5 = findViewById(R.id.dice5);
        wurfCounter = findViewById(R.id.wurfCounter);
        wurfCounter.setText(String.valueOf(currentPlayer.getThrowCounter()));
        playerTable = findViewById(R.id.zumSpielblock);
        diceViewList.add(dice1);
        diceViewList.add(dice2);
        diceViewList.add(dice3);
        diceViewList.add(dice4);
        diceViewList.add(dice5);
        int count = 1;
        for (ImageView diceView : diceViewList) {
            diceView.setOnClickListener(v -> selectDice(diceView));
            Log.d("Setting tag for DiceView ","dice"+count);
            diceView.setTag(R.id.initalID, "dice" + count);
            int id = getResources().getIdentifier(currentPlayer.getPlayersDiceList().get(count - 1).getPictureURL(), "drawable", getPackageName());
            diceView.setImageResource(id);
            count++;
        }
    }
    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }
}