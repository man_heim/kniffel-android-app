package com.example.kniffel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.webianks.library.scroll_choice.ScrollChoice;

import java.util.ArrayList;
import java.util.List;

import GameManager.GameController;

public class StartActivity extends AppCompatActivity {

    private ScrollChoice scrollChoice;
    private List<String> scrollList = new ArrayList<>();
    private List<EditText> playerNameFields = new ArrayList<>();
    private Button buttonStart, buttonExit;
    static GameController gameController;
    private static List<String> nameList = new ArrayList<>();
    public  static String PACKAGENAME;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        PACKAGENAME = getApplicationContext().getPackageName();
        Log.d("MainActicity"," Packagename: " + PACKAGENAME);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initList();
        scrollChoice.addItems(scrollList, 0);
        scrollChoice.setOnItemSelectedListener((scrollChoice, position, name) -> showPlayers(Integer.parseInt(name)));
        buttonStart.setOnClickListener(v -> {
            playerNameFields.forEach(nameField -> nameList.add(nameField.getText().toString()));
            gameController = new GameController(nameList);
            DiceThrowActivity.gameController = gameController;
            Intent intent = new Intent(this, DiceThrowActivity.class);
            startActivity(intent);
        });
        buttonExit.setOnClickListener(v-> finish());
    }


    public static GameController getGameController() {
        return gameController;
    }

    /**
     * Initalizing the ScrollList
     */
    private void initList() {
        for (int i = 2; i < 9; i++) {
            scrollList.add(String.valueOf(i));
        }
    }

    /**
     * Showing the fields for name input of the players
     * @param playerAmount amount of Players to play the game (2-8)
     */
    private void showPlayers(int playerAmount) {
        for (int i = 0; i < playerAmount; i++) {
            playerNameFields.get(i).setVisibility(View.VISIBLE);
            playerNameFields.get(i).setEnabled(true);
        }
        for (int i = playerAmount; i < 8; i++) {
            playerNameFields.get(i).setVisibility(View.INVISIBLE);
            playerNameFields.get(i).setEnabled(false);
        }
    }

    /**
     * Initalizing the views
     */
    private void initViews() {
        scrollChoice = findViewById(R.id.scrollChoice);
        EditText namePlayer1 = findViewById(R.id.namePlayer1);
        playerNameFields.add(namePlayer1);
        EditText namePlayer2 = findViewById(R.id.namePlayer2);
        playerNameFields.add(namePlayer2);
        EditText namePlayer3 = findViewById(R.id.namePlayer3);
        playerNameFields.add(namePlayer3);
        EditText namePlayer4 = findViewById(R.id.namePlayer4);
        playerNameFields.add(namePlayer4);
        EditText namePlayer5 = findViewById(R.id.namePlayer5);
        playerNameFields.add(namePlayer5);
        EditText namePlayer6 = findViewById(R.id.namePlayer6);
        playerNameFields.add(namePlayer6);
        EditText namePlayer7 = findViewById(R.id.namePlayer7);
        playerNameFields.add(namePlayer7);
        EditText namePlayer8 = findViewById(R.id.namePlayer8);
        playerNameFields.add(namePlayer8);
        buttonStart = findViewById(R.id.buttonNeuesSpiel);
        buttonExit = findViewById(R.id.buttonBeenden2);
    }

}
