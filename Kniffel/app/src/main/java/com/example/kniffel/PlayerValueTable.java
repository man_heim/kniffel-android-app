package com.example.kniffel;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import DiceController.Dice;
import GameManager.GameController;
import GameManager.ResultChecker;
import PlayerController.Player;

public class PlayerValueTable extends AppCompatActivity implements DeletingDialog.OnDialogAcceptedListener {

    private TextView spielBlock, einserPoints, zweierPoints, dreierPoints, viererPoints, fuenferPoints, sechserPoints, dreierPaschPoints, viererPaschPoints, fullHousePoints, kleineStrassePoints, grosseStrassePoints, kniffelPoints, chancePoints, viewPunkte1, viewPunkte2, viewPunkteFinal, viewBonus;
    private List<TextView> pointsViewList = new ArrayList<>();
    private TextView currentPointsSelected;
    private ResultChecker resultChecker = new ResultChecker();
    private RadioGroup radioGroup;
    private Button confirm;
    private Button delete;
    private boolean safeAllowed = false;


    private GameController gameController = DiceThrowActivity.getGameController();
    private Player playerOwnerOfThisTable = gameController.getThisPlayer();
    private List<ImageView> currentThrowDices = new ArrayList<>();
    private List<RadioButton> radioButtonList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        radioGroup = new RadioGroup(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_value_table);
        initialize();
        setPlayerName();
        setDiceView();
        setPointView(getBaseContext());
        radioButtonHandling();
    }

    /**
     * Handler for Clicks on RadioButtons
     */
    private void radioButtonHandling() {
        radioButtonList.forEach(radioButton -> radioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                radioButtonList.forEach(radioButton1 -> {
                    if (radioButton1 != buttonView) {
                        radioButton1.setChecked(false);
                    } else {
                        radioButton1.setChecked(true);
                        changePoints(radioButton1);
                    }
                });
            }
        }));
        delete.setVisibility(View.VISIBLE);
        delete.setClickable(true);
        confirm.setVisibility(View.VISIBLE);
        confirm.setClickable(true);
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> changePoints(findViewById(radioGroup.getCheckedRadioButtonId())));
    }

    /**
     * Setting The Points for the radio chosen radio button
     * when the result ist allowed
     *
     * @param radioButton Which points should be set
     */
    @SuppressLint("SetTextI18n")
    private void changePoints(RadioButton radioButton) {
        String name = getResources().getResourceEntryName(radioButton.getId());
        switch (name) {
            case "einserRadioButton":
                calculatingDices(1, einserPoints);
                break;
            case "zweierRadioButton":
                calculatingDices(2, zweierPoints);
                break;
            case "dreierRadioButton":
                calculatingDices(3, dreierPoints);
                break;
            case "viererRadioButton":
                calculatingDices(4, viererPoints);
                break;
            case "fuenferRadioButton":
                calculatingDices(5, fuenferPoints);
                break;
            case "sechserRadioButton":
                calculatingDices(6, sechserPoints);
                break;
            case "dreierPaschRadioButton":
                if (currentPointsSelected != null) {
                    currentPointsSelected.setText("0");
                    currentPointsSelected.setPaddingRelative(15, 0, 15, 0);
                }
                currentPointsSelected = dreierPaschPoints;
                int result = resultChecker.checkPasch(new ArrayList<>(playerOwnerOfThisTable.getPlayersDiceList()), 3);
                Log.d("PlayerValueTable", "Result: " + result);
                if (result != 0) {
                    dreierPaschPoints.setText(Integer.toString(result));
                    safeAllowed = true;

                }
                break;
            case "viererPaschRadioButton":
                if (currentPointsSelected != null) {
                    currentPointsSelected.setText("0");
                    currentPointsSelected.setPaddingRelative(15, 0, 15, 0);
                }
                currentPointsSelected = viererPaschPoints;
                int result2 = resultChecker.checkPasch(new ArrayList<>(playerOwnerOfThisTable.getPlayersDiceList()), 4);
                if (result2 != 0) {
                    viererPaschPoints.setText(Integer.toString(result2));
                    safeAllowed = true;

                }
                break;
            case "fullHouseRadioButton":
                System.out.println(resultChecker.checkFullHouse(new ArrayList<>(playerOwnerOfThisTable.getPlayersDiceList())));
                if (currentPointsSelected != null) {
                    currentPointsSelected.setText("0");
                    chancePoints.setPaddingRelative(15, 0, 15, 0);
                    safeAllowed = true;
                }
                currentPointsSelected = fullHousePoints;
                if (resultChecker.checkFullHouse(new ArrayList<>(playerOwnerOfThisTable.getPlayersDiceList()))) {
                    fullHousePoints.setText(Integer.toString(25));
                    fullHousePoints.setPaddingRelative(10, 0, 7, 0);
                    safeAllowed = true;
                }
                break;
            case "kleineStrasseRadioButton":
                if (currentPointsSelected != null) {
                    currentPointsSelected.setText("0");
                    currentPointsSelected.setPaddingRelative(15, 0, 15, 0);
                }
                currentPointsSelected = kleineStrassePoints;
                if (resultChecker.checkStreet(new ArrayList<>(playerOwnerOfThisTable.getPlayersDiceList()), 4)) {
                    kleineStrassePoints.setText(Integer.toString(30));
                    kleineStrassePoints.setPaddingRelative(10, 0, 7, 0);
                    safeAllowed = true;
                }
                break;
            case "grosseStrasseRadioButton":
                if (currentPointsSelected != null) {
                    currentPointsSelected.setText("0");
                    currentPointsSelected.setPaddingRelative(15, 0, 15, 0);
                }
                currentPointsSelected = grosseStrassePoints;
                if (resultChecker.checkStreet(new ArrayList<>(playerOwnerOfThisTable.getPlayersDiceList()), 5)) {
                    grosseStrassePoints.setText(Integer.toString(40));
                    grosseStrassePoints.setPaddingRelative(10, 0, 7, 0);
                    safeAllowed = true;
                }
                break;
            case "kniffelRadioButton":
                if (currentPointsSelected != null) {
                    currentPointsSelected.setText("0");
                    currentPointsSelected.setPaddingRelative(15, 0, 15, 0);
                }
                currentPointsSelected = kniffelPoints;
                if (resultChecker.checkKniffel(new ArrayList<>(playerOwnerOfThisTable.getPlayersDiceList()))) {
                    kniffelPoints.setText(Integer.toString(50));
                    kniffelPoints.setPaddingRelative(10, 0, 7, 0);
                    safeAllowed = true;
                }
                break;
            case "chanceRadioButton":
                if (currentPointsSelected != null) {
                    currentPointsSelected.setText("0");
                    currentPointsSelected.setPaddingRelative(15, 0, 15, 0);
                }
                int value = playerOwnerOfThisTable.getPlayersDiceList().stream().mapToInt(Dice::getValue).sum();
                chancePoints.setText(Integer.toString(value));
                if (value >= 10)
                    chancePoints.setPaddingRelative(10, 0, 7, 0);
                currentPointsSelected = chancePoints;
                safeAllowed = true;
                break;


        }
    }

    /**
     * the chosen Field gets deleted
     */
    private void deletingFields() {
        if (currentPointsSelected != null) {
            DeletingDialog deletingDialog = new DeletingDialog();
            String figureID = getResources().getResourceEntryName(currentPointsSelected.getId());
            String temp = figureID.replace("Points", "");
            deletingDialog.setFieldName(temp);
            deletingDialog.show(getSupportFragmentManager(), "streichenDialog");
        }
    }

    /**
     * Going back to the DiceThrow-Scene when a field is deleted
     *
     * @param accepted shows whether the user accepted the dialog to delete a field
     */

    @Override
    public void dialogAccepted(boolean accepted) {
        if (accepted) {
            playerOwnerOfThisTable.getTablePointController().setFigureValue(getResources().getResourceEntryName(currentPointsSelected.getId()), -1, (String) currentPointsSelected.getTag());
            playerOwnerOfThisTable.resetThrowCounter();
            Intent intent = new Intent(this, DiceThrowActivity.class);
            startActivity(intent);
            gameController.nextPlayer();
            if (gameController.checkFinished(pointsViewList, getApplicationContext())) {
                Intent intent2 = new Intent(this, WinnerScreenControl.class);
                startActivity(intent2);
            }
        }
    }

    /**
     * The Points for the chosen field get saved
     */
    private void savingPoints() {
        if (safeAllowed) {
            Log.d("Speichere punkte", getResources().getResourceName(currentPointsSelected.getId()));
            String nameToStore = getResources().getResourceEntryName(currentPointsSelected.getId());
            playerOwnerOfThisTable.getTablePointController().setFigureValue(nameToStore, Integer.parseInt((String) currentPointsSelected.getText()), ((String) currentPointsSelected.getTag()));
            playerOwnerOfThisTable.resetThrowCounter();
            Intent intent = new Intent(this, DiceThrowActivity.class);
            startActivity(intent);
            gameController.nextPlayer();
        }
        if (gameController.checkFinished(pointsViewList, getApplicationContext())) {
            Intent intent2 = new Intent(this, WinnerScreenControl.class);
            startActivity(intent2);
        }
    }


    /**
     * Sets all Points in the TextView according to the points of the current player
     * in this category, also disables the RadioButtons for categorys that are already set
     *
     * @param context Context of the current activity
     */
    @SuppressLint({"ResourceAsColor", "SetTextI18n"})
    private void setPointView(Context context) {
        pointsViewList.forEach(pointsView -> {
            int pointsForFigure = playerOwnerOfThisTable.getTablePointController().getPointsForFigure(context.getResources().getResourceEntryName(pointsView.getId()));
            if (pointsForFigure != 0) {
                if (pointsForFigure == -1) {
                    pointsView.setText("X");
                } else {
                    pointsView.setText(String.valueOf(pointsForFigure));
                }
                String nameToStore = getResources().getResourceEntryName(pointsView.getId());
                nameToStore = nameToStore.replace("Points", "RadioButton");
                findViewById(getResources().getIdentifier(nameToStore, "id", getPackageName())).setClickable(false);
            }
        });
        int points1 = playerOwnerOfThisTable.getTablePointController().getPointsPart1();
        int points2 = playerOwnerOfThisTable.getTablePointController().getPointsPart2();
        if (playerOwnerOfThisTable.getTablePointController().isBonus()) {
            viewBonus.setText("Bonus: " + points1);
            viewBonus.setTextColor(R.color.green);
            viewPunkte1.setText("Punkte Teil 1: " + (points1 - 35));
        } else {
            viewPunkte1.setText(String.valueOf(points1));
        }
        viewPunkte2.setText("Punkte Teil 2: " + points2);
        viewPunkteFinal.setText("Gesamtpunktzahl: " + (points1 + points2));
        currentPointsSelected = null;
    }


    /**
     * Exit Handler for the Exit-Button (x)
     */
    private void exitToDiceThow() {
        Intent intent = new Intent(this, DiceThrowActivity.class);
        startActivity(intent);
    }

    /**
     * Calculating the Points for all categories of the first section
     *
     * @param i      Value of the dice, for which the sum should be calculated
     * @param points TextView to put in the result of the calculation
     */
    @SuppressLint("SetTextI18n")                                        //TODO String.format
    private void calculatingDices(int i, TextView points) {
        playerOwnerOfThisTable.getPlayersDiceList().forEach(dice -> Log.d("dice Wert", String.valueOf(dice.getValue())));
        int multiplier = (int) playerOwnerOfThisTable.getPlayersDiceList().stream().filter(dice -> dice.getValue() == i).count();
        if (currentPointsSelected != null) {
            currentPointsSelected.setText("0");
            currentPointsSelected.setPaddingRelative(15, 0, 15, 0);
        }
        safeAllowed = true;
        currentPointsSelected = points;
        points.setText(Integer.toString(i * multiplier));
        if ((i * multiplier) >= 10)
            points.setPaddingRelative(10, 0, 7, 0);

    }

    /**
     * Initalizing the View of the current thrown dices
     */
    private void setDiceView() {
        AtomicInteger listCount = new AtomicInteger();
        playerOwnerOfThisTable.getPlayersDiceList().forEach(dice -> {
            Log.d("Set Dice View", dice.getPictureURL());
            int id = getResources().getIdentifier(dice.getPictureURL(), "drawable", getPackageName());
            Log.d("Set Dice View", String.valueOf(id));
            currentThrowDices.get(listCount.get()).setImageResource(id);
            listCount.getAndIncrement();
        });
    }

    /**
     * Setting the NameView with the Name of the current player
     */
    @SuppressLint({"ResourceType", "SetTextI18n"})
    private void setPlayerName() {
        spielBlock.setText("Spielblock: " + gameController.getThisPlayer().getName());
    }

    /**
     * Initializing views
     */
    private void initialize() {
        spielBlock = findViewById(R.id.spielBlock);
        Button exitToDiceThrow = findViewById(R.id.exitToDiceThrow);
        exitToDiceThrow.setOnClickListener(v -> exitToDiceThow());
        ImageView wurf_1 = findViewById(R.id.wurf_1);
        currentThrowDices.add(wurf_1);
        ImageView wurf_2 = findViewById(R.id.wurf_2);
        currentThrowDices.add(wurf_2);
        ImageView wurf_3 = findViewById(R.id.wurf_3);
        currentThrowDices.add(wurf_3);
        ImageView wurf_4 = findViewById(R.id.wurf_4);
        currentThrowDices.add(wurf_4);
        ImageView wurf_5 = findViewById(R.id.wurf_5);
        currentThrowDices.add(wurf_5);
        RadioButton einserRadioButton = findViewById(R.id.einserRadioButton);
        radioButtonList.add(einserRadioButton);
        RadioButton zweierRadioButton = findViewById(R.id.zweierRadioButton);
        radioButtonList.add(zweierRadioButton);
        RadioButton dreierRadioButton = findViewById(R.id.dreierRadioButton);
        radioButtonList.add(dreierRadioButton);
        RadioButton viererRadioButton = findViewById(R.id.viererRadioButton);
        radioButtonList.add(viererRadioButton);
        RadioButton fuenferRadioButton = findViewById(R.id.fuenferRadioButton);
        radioButtonList.add(fuenferRadioButton);
        RadioButton sechserRadioButton = findViewById(R.id.sechserRadioButton);
        radioButtonList.add(sechserRadioButton);
        RadioButton dreierPaschRadioButton = findViewById(R.id.dreierPaschRadioButton);
        radioButtonList.add(dreierPaschRadioButton);
        RadioButton viererPaschRadioButton = findViewById(R.id.viererPaschRadioButton);
        radioButtonList.add(viererPaschRadioButton);
        radioButtonList.add(viererPaschRadioButton);
        RadioButton fullHouseRadioButton = findViewById(R.id.fullHouseRadioButton);
        radioButtonList.add(fullHouseRadioButton);
        RadioButton kleineStrasseRadioButton = findViewById(R.id.kleineStrasseRadioButton);
        radioButtonList.add(kleineStrasseRadioButton);
        RadioButton grosseStrasseRadioButton = findViewById(R.id.grosseStrasseRadioButton);
        radioButtonList.add(grosseStrasseRadioButton);
        RadioButton kniffelRadioButton = findViewById(R.id.kniffelRadioButton);
        radioButtonList.add(kniffelRadioButton);
        RadioButton chanceRadioButton = findViewById(R.id.chanceRadioButton);
        radioButtonList.add(chanceRadioButton);
        einserPoints = findViewById(R.id.einserPoints);
        pointsViewList.add(einserPoints);
        zweierPoints = findViewById(R.id.zweierPoints);
        pointsViewList.add(zweierPoints);
        dreierPoints = findViewById(R.id.dreierPoints);
        pointsViewList.add(dreierPoints);
        viererPoints = findViewById(R.id.viererPoints);
        pointsViewList.add(viererPoints);
        fuenferPoints = findViewById(R.id.fuenferPoints);
        pointsViewList.add(fuenferPoints);
        sechserPoints = findViewById(R.id.sechserPoints);
        pointsViewList.add(sechserPoints);
        dreierPaschPoints = findViewById(R.id.dreierPaschPoints);
        pointsViewList.add(dreierPaschPoints);
        viererPaschPoints = findViewById(R.id.viererPaschPoints);
        pointsViewList.add(viererPaschPoints);
        fullHousePoints = findViewById(R.id.fullHousePoints);
        pointsViewList.add(fullHousePoints);
        kleineStrassePoints = findViewById(R.id.kleineStrassePoints);
        pointsViewList.add(kleineStrassePoints);
        grosseStrassePoints = findViewById(R.id.grosseStrassePoints);
        pointsViewList.add(grosseStrassePoints);
        kniffelPoints = findViewById(R.id.kniffelPoints);
        pointsViewList.add(kniffelPoints);
        chancePoints = findViewById(R.id.chancePoints);
        pointsViewList.add(chancePoints);
        confirm = findViewById(R.id.bestätigen);
        confirm.setOnClickListener(v -> savingPoints());
        delete = findViewById(R.id.streichen);
        delete.setOnClickListener(v -> deletingFields());
        viewPunkte1 = findViewById(R.id.viewPunkte1);
        viewPunkte2 = findViewById(R.id.viewPunkte2);
        viewPunkteFinal = findViewById(R.id.viewPunkteFinal);
        viewBonus = findViewById(R.id.viewBonus);
    }
}

