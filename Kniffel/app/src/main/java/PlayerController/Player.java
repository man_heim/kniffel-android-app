package PlayerController;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import DiceController.Dice;

public class Player {
    private int throwCounter =3;
    private String name;
    private TablePointController tablePointController;
    private List<Dice> playersDiceList = new ArrayList<Dice>(Arrays.asList(new Dice("dice1"), new Dice("dice2"), new Dice("dice3"), new Dice("dice4"), new Dice("dice5")));;

    public Player(String name){
        this.name = name;
        tablePointController = new TablePointController();
    }

    public int getThrowCounter() {
        return throwCounter;
    }
    public void reduceWurfCounter(){
        throwCounter--;
    }

    /**
     * Setting ThrowCounterBack to 3
     */
    public void resetThrowCounter(){
        throwCounter =3;
    }
    public TablePointController getTablePointController() {
        return tablePointController;
    }

    public String getName() {
        return name;
    }

    public List<Dice> getPlayersDiceList() {
        return playersDiceList;
    }

}
