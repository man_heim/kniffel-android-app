package PlayerController;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TablePointController {

    private Map<String, Figure> figuresList = new HashMap<>();
    private int totalPoints = 0;
    private int pointsPart1 = 0;
    private int pointsPart2 = 0;
    private boolean bonus = false;

    TablePointController() {
        figuresList.put("einserPoints", new Figure(0, false));
        figuresList.put("zweierPoints", new Figure(0, false));
        figuresList.put("dreierPoints", new Figure(0, false));
        figuresList.put("viererPoints", new Figure(0, false));
        figuresList.put("fuenferPoints", new Figure(0, false));
        figuresList.put("sechserPoints", new Figure(0, false));
        figuresList.put("dreierPaschPoints", new Figure(0, false));
        figuresList.put("viererPaschPoints", new Figure(0, false));
        figuresList.put("fullHousePoints", new Figure(0, false));
        figuresList.put("kleineStrassePoints", new Figure(0, false));
        figuresList.put("grosseStrassePoints", new Figure(0, false));
        figuresList.put("kniffelPoints", new Figure(0, false));
        figuresList.put("chancePoints", new Figure(0, false));
    }



    public int getPointsForFigure(String keyValue) {
        if (figuresList.containsKey(keyValue)) {
            Figure figure = figuresList.get(keyValue);
            assert figure != null;
            if (figure.set) {
                return figure.points;
            }
        }
        return 0;
    }


    public int getPointsPart1() {
        return pointsPart1;
    }

    public int getPointsPart2() {
        return pointsPart2;
    }

    public boolean isBonus() {
        return bonus;
    }

    /**
     * Setting the Values of a Figure
     * @param figureToSet ID of the figure to set
     * @param points points of the figure to set
     * @param tag in which part is the figure
     */
    public void setFigureValue(String figureToSet, int points, String tag) {
        Log.d("Table Point Controller", "Figure to Set: " + figureToSet);
        Objects.requireNonNull(figuresList.get(figureToSet)).points = points;
        Log.d("TablePointController", figureToSet + " Set to: " + Objects.requireNonNull(figuresList.get(figureToSet)).points);
        Objects.requireNonNull(figuresList.get(figureToSet)).set = true;
        if (points != -1) {
            if (tag.equals("1")) {
                pointsPart1 += points;
            } else if (tag.equals("2")) {
                pointsPart2 += points;
            }
            if (pointsPart1 >= 63) {
                pointsPart1 += 35;
                bonus = true;
            }
            totalPoints = pointsPart1 + pointsPart2;
        }
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    /**
     * Checking if all Values are set
     * @param pointsViewList  TextView with the id to search in the users Figure List
     * @param applicationContext Context to search for id's
     * @return true if all Values are set
     */
    public boolean allValuesSet(List<TextView> pointsViewList, Context applicationContext) {
        for (TextView pointsView : pointsViewList) {
            Log.d("TablePointController",applicationContext.getResources().getResourceEntryName(pointsView.getId()));
            if (!Objects.requireNonNull(figuresList.get(applicationContext.getResources().getResourceEntryName(pointsView.getId()))).set) {
                return false;
            }
        }
        return true;

    }

    private static class Figure {
        int points;
        boolean set;

        Figure(int points, boolean set) {
            this.points = points;
            this.set = set;
        }
    }
}
